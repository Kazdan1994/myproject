<?php

namespace App\Controller;

use App\Entity\Product;
use Faker\Factory;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    /**
     * @var \Faker\Generator
     */
    private $faker;

    public function __construct()
    {
        $this->faker = Factory::create();
    }

    /**
     * @Route("/products", name="products")
     */
    public function index()
    {
        try {
            $products = $this
                ->getDoctrine()
                ->getRepository(Product::class)
                ->findAll();
        } catch (\Exception $e) {
            return $this->render('product/index.html.twig', [
                'products' => [],
                'errors' => $e->getMessage()
            ]);
        }

        return $this->render('product/index.html.twig', [
            'products' => $products
        ]);
    }

    /**
     * @Route("/product/new", name="newProduct")
     */
    public function create() : Response
    {
        try {
            $entityManager = $this->getDoctrine()->getManager();
            $product = new Product();
            $product->setName($this->faker->text(10));
            $product->setPrice($this->faker->randomFloat(2));

            $entityManager->persist($product);
            $entityManager->flush();
        } catch (\Exception $e) {
            return $this->redirectToRoute("products", [
                'products' => [],
                'errors' => $e->getMessage()
            ]);
        }
        return $this->redirectToRoute("products", [
            'products' => []
        ]);
    }
}
